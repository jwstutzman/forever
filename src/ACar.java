/**
 * common workflow in java:
 * 
 * create an interface (lets us program in general)
 * create an abstract class (lets us code shared functionality
 * create concrete classes
 * 
 * abstract:
 * -cann9ot be instantiated
 * -concrete classes that inherit from the abstract class "must" implement 
 * all the abstract methods
 * -Abstract subclasses may or may not implement the abstract methods
 * 
 * implements means it inherits from an interface
 * you can implement as many interfaces as you want
 * 
 * everything extends object
 * 
 * @author unouser
 *
 */
public abstract class ACar implements ICar, java.io.Serializable{
	/** the make of the car*/
	private String make;
	/** the model of the car*/
	private String model;
	/** the year of the car*/
	private int year;
	/** the miles of the car*/
	private int mileage;
	
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}
	
	@Override
	public String toString() {
		return getMake() + " " + getModel();
	}

	
	
	
}
