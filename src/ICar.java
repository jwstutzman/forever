/**
 * different types in Java:
 * class
 * enum
 * primitives
 * interfaces
 * 
 * a class is a bombination of data and methods
 * 
 * an interface only defines methods that its subclasses will have
 * by definition, everythin in an interface is public
 * 
 * 
 * @author unouser
 *
 */
public interface ICar 
{
	String getMake();
	String getModel();
	int getYear();
	int getMileage();
}
